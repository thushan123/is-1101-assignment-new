//k.k.t.dananjana-202014(2019/IS/016)
#include <stdio.h>
struct student {
    char name[25];
    int id;
    char subject[10];
    float marks;
} ;

int main() {
	struct student s[5];
    int i;
    printf("Enter information of students:\n");

    for (i=0;i<5; ++i) {
        
        printf("\nStudent id number: ");
        scanf("%d",&s[i].id);
        printf("Enter first name: ");
        scanf("%s", s[i].name);
        printf("Enter subject name: ");
        scanf("%s",s[i].subject);
        printf("Enter marks: ");
        scanf("%f",&s[i].marks);
    }
    printf("Displaying Information:\n\n");

	    for (i=0;i<5;++i) {
       
        printf("\n %d\t  %s\t  %s\t  %.2f",s[i].id,s[i].name,s[i].subject,s[i].marks);
        
    }
    return 0;
}
